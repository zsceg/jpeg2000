FROM alpine as jp2k

RUN apk update && apk --no-cache add cmake make g++         libpng-dev

COPY src /src
RUN mkdir -v /src/build
WORKDIR /src/build

RUN cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr && make
RUN make install

RUN find /src/build/bin

FROM scratch

COPY --from=jp2k /src/build/bin/lib* /usr/lib/
COPY --from=jp2k /src/build/bin/opj* /usr/bin/
